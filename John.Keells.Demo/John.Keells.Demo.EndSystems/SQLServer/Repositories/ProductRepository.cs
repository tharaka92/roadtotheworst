﻿using John.Keells.Demo.Domain.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using John.Keells.Demo.Domain.DataEntities;
using John.Keells.Demo.EndSystems.SQLServer.StoredProcedures;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using System.Data.Common;

namespace John.Keells.Demo.EndSystems.SQLServer.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public ProductRepository() { }

        public async Task<IEnumerable<Product>> Retrieve(int? productId, string productCode)
        {
            try
            {
                SPRetrieveProduct parameters = new SPRetrieveProduct();
                parameters.ProductId = productId.HasValue ? productId : null;
                parameters.ProductCode = string.IsNullOrEmpty(productCode) ? null : productCode;

                using (DbConnection connection = DBConnectionFactory.GetConnection())
                {
                    connection.Open();
                    return await connection.QueryAsync<Product>(sql: parameters.GetName(), param: parameters, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Create(Product productToCreate)
        {
            try
            {
                SPInsertProduct parameters = new SPInsertProduct();
                parameters.ProductName = productToCreate.ProductName;
                parameters.ProductCode = productToCreate.ProductCode;
                parameters.Country = productToCreate.Country;
                parameters.Price = productToCreate.Price;
                parameters.IsDeleted = false;

                using (DbConnection connection = DBConnectionFactory.GetConnection())
                {
                    connection.Open();
                    int affectedRows = await connection.ExecuteAsync(sql: parameters.GetName(), param: parameters, commandType: CommandType.StoredProcedure);
                    if (affectedRows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(Product productToUpdate)
        {
            try
            {
                SPUpdateProduct parameters = new SPUpdateProduct();
                parameters.ProductId = productToUpdate.ProductId;
                parameters.ProductName = productToUpdate.ProductName;
                parameters.ProductCode = productToUpdate.ProductCode;
                parameters.Country = productToUpdate.Country;
                parameters.Price = productToUpdate.Price;

                using (DbConnection connection = DBConnectionFactory.GetConnection())
                {
                    connection.Open();
                    int affectedRows = await connection.ExecuteAsync(sql: parameters.GetName(), param: parameters, commandType: CommandType.StoredProcedure);
                    if (affectedRows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Delete(int productId)
        {
            try
            {
                SPDeleteProduct parameters = new SPDeleteProduct();
                parameters.ProductId = productId;

                using (DbConnection connection = DBConnectionFactory.GetConnection())
                {
                    connection.Open();
                    int affectedRows = await connection.ExecuteAsync(sql: parameters.GetName(), param: parameters, commandType: CommandType.StoredProcedure);
                    if (affectedRows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
