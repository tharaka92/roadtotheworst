﻿using John.Keells.Demo.Domain.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.EndSystems.SQLServer.StoredProcedures
{
    public class SPDeleteProduct : SP
    {
        public SPDeleteProduct() { }

        public int ProductId { get; set; }
    }
}
