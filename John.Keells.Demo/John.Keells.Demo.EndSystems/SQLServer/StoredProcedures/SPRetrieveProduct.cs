﻿using John.Keells.Demo.Domain.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.EndSystems.SQLServer.StoredProcedures
{
    public class SPRetrieveProduct : SP
    {
        public SPRetrieveProduct() { }

        public int? ProductId { get; set; }
        public string ProductCode { get; set; }
    }
}
