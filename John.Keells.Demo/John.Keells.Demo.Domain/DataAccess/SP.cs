﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.Domain.DataAccess
{
    public abstract class SP
    {
        public virtual string GetName()
        {
            return GetType().Name;
        } 
    }
}
