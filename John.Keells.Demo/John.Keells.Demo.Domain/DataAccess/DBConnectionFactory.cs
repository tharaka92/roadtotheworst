﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.Domain.DataAccess
{
    public class DBConnectionFactory
    {
        public static DbConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["JohnKeellsDbConnection"].ConnectionString;
            return connection;
        }
    }
}
