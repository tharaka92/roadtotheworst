﻿using John.Keells.Demo.Domain.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.Domain.DataAccess
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> Retrieve(int? productId, string productCode);
        Task<bool> Create(Product productToCreate);
        Task<bool> Update(Product productToUpdate);
        Task<bool> Delete(int productId);
    }
}
