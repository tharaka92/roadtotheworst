﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John.Keells.Demo.Domain.DataEntities
{
    public class Product
    {
        public Product() { }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Country { get; set; }
        public decimal Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}
