﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace John.Keells.Demo.MVCClient.DTOs
{
    public class ProductDTO
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Country { get; set; }
        public decimal Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}