﻿using AutoMapper;
using John.Keells.Demo.MVCClient.DTOs;
using John.Keells.Demo.MVCClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace John.Keells.Demo.MVCClient.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterBindings()
        {
            Mapper.Initialize(config => 
            {
                config.CreateMap<ProductDTO, ProductModel>();
                config.CreateMap<ProductModel, ProductDTO>();
            });
        }
    }
}