﻿using System.Web;
using System.Web.Mvc;

namespace John.Keells.Demo.MVCClient
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
