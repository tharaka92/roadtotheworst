﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(John.Keells.Demo.MVCClient.Startup))]
namespace John.Keells.Demo.MVCClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
