﻿using John.Keells.Demo.MVCClient.DTOs;
using John.Keells.Demo.MVCClient.HTTPClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace John.Keells.Demo.MVCClient.HTTPServices
{
    public class ProductService
    {
        public ProductService() { }

        public async Task<IEnumerable<ProductDTO>> GetProducts()
        {
            try
            {
                using (HttpClient client = HTTPClientFactory.GetClient())
                {
                    var apiResponse = await client.GetAsync("api/Product");
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        var responseContent = apiResponse.Content.ReadAsStringAsync().Result;
                        return JsonConvert.DeserializeObject<IEnumerable<ProductDTO>>(responseContent);
                    }
                    else
                    {
                        throw new Exception(apiResponse.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ProductDTO> GetProductById(int productId)
        {
            try
            {
                using (HttpClient client = HTTPClientFactory.GetClient())
                {
                    var apiResponse = await client.GetAsync("api/Product/" + productId);
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        //var responseContent = apiResponse.Content.ReadAsStringAsync().Result;
                        //return JsonConvert.DeserializeObject<ProductDTO>(responseContent);
                        return apiResponse.Content.ReadAsAsync<ProductDTO>().Result;
                    }
                    else
                    {
                        throw new Exception(apiResponse.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> PostProduct(ProductDTO productToPost)
        {
            try
            {
                using (HttpClient client = HTTPClientFactory.GetClient())
                {
                    var apiResponse = await client.PostAsJsonAsync("api/Product/", productToPost);
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(apiResponse.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateProduct(ProductDTO productToUpdate)
        {
            try
            {
                using (HttpClient client = HTTPClientFactory.GetClient())
                {
                    var apiResponse = await client.PutAsJsonAsync("api/Product/", productToUpdate);
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(apiResponse.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteProduct(int productId)
        {
            try
            {
                using (HttpClient client = HTTPClientFactory.GetClient())
                {
                    var apiResponse = await client.DeleteAsync("api/Product/" + productId);
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(apiResponse.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}