﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace John.Keells.Demo.MVCClient.Controllers
{
    public class ExceptionController : Controller
    {
        // GET: Exception
        public ActionResult Index()
        {
            ViewBag.ExceptionText = TempData["ExceptionText"].ToString();
            return View();
        }

        // GET: Exception/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Exception/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Exception/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Exception/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Exception/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Exception/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Exception/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
