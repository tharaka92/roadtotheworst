﻿using AutoMapper;
using John.Keells.Demo.MVCClient.DTOs;
using John.Keells.Demo.MVCClient.HTTPServices;
using John.Keells.Demo.MVCClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace John.Keells.Demo.MVCClient.Controllers
{
    public class ProductController : Controller
    {
        ProductService productService = new ProductService();

        // GET: Product
        public async Task<ActionResult> Index()
        {
            try
            {
                IEnumerable<ProductDTO> productDtos = await productService.GetProducts();
                if (productDtos != null)
                {
                    IEnumerable<ProductModel> productModels = Mapper.Map<IEnumerable<ProductModel>>(productDtos);
                    return View(productModels);
                }
                return View();
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public async Task<ActionResult> Create(ProductModel productToCreate)
        {
            try
            {
                if (productToCreate != null)
                {
                    ProductDTO productDtoToPost = Mapper.Map<ProductDTO>(productToCreate);
                    await productService.PostProduct(productDtoToPost);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }

        // GET: Product/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                ProductDTO productDtoToEdit = await productService.GetProductById(id);
                if (productDtoToEdit != null)
                {
                    ProductModel productModelToEdit = Mapper.Map<ProductModel>(productDtoToEdit);
                    return View(productModelToEdit);
                }
                return View();
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }

        // POST: Product/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(ProductModel productModelToEdit)
        {
            try
            {
                if (productModelToEdit != null)
                {
                    ProductDTO productDtoToEdit = Mapper.Map<ProductDTO>(productModelToEdit);
                    await productService.UpdateProduct(productDtoToEdit);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }

        // GET: Product/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                ProductDTO productDtoToDelete = await productService.GetProductById(id);
                if (productDtoToDelete != null)
                {
                    ProductModel productModelToDelete = Mapper.Map<ProductModel>(productDtoToDelete);
                    return View(productModelToDelete);
                }
                return View();
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }

        // POST: Product/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(ProductModel productModelToDelete)
        {
            try
            {
                if (productModelToDelete != null)
                {
                    await productService.DeleteProduct(productModelToDelete.ProductId);
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["ExceptionText"] = "Exception Occured, Please Contact the Administrator";
                return RedirectToAction("Index", "Exception");
            }
        }
    }
}
