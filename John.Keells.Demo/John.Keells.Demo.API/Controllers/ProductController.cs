﻿using AutoMapper;
using John.Keells.Demo.Contract.Models;
using John.Keells.Demo.Domain.DataAccess;
using John.Keells.Demo.Domain.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace John.Keells.Demo.API.Controllers
{
    public class ProductController : ApiController
    {
        private IProductRepository ProductRepository;

        public ProductController(IProductRepository productRepositoy)
        {
            if (productRepositoy == null)
            {
                throw new ArgumentNullException("productRepositoy");
            }

            this.ProductRepository = productRepositoy;
        }
        // GET: api/Product
        public async Task<IEnumerable<ProductModel>> Get()
        {
            try
            {
                IEnumerable<Product> products = await ProductRepository.Retrieve(null, null);
                if (products != null)
                {
                    IEnumerable<ProductModel> productModels = Mapper.Map<IEnumerable<ProductModel>>(products);
                    return productModels;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("Internal Server Error"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        // GET: api/Product/5
        public async Task<ProductModel> Get(int id)
        {
            try
            {
                IEnumerable<Product> product = await ProductRepository.Retrieve(id, null);
                if (product != null)
                {
                    ProductModel productModel = Mapper.Map<ProductModel>(product.SingleOrDefault());
                    return productModel;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("Internal Server Error"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        // POST: api/Product
        public async Task<bool> Post(ProductModel productModelToCreate)
        {
            try
            {
                if (productModelToCreate != null)
                {
                    Product productEntityToCreate = Mapper.Map<Product>(productModelToCreate);
                    return await ProductRepository.Create(productEntityToCreate);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No Content in the Request"),
                        ReasonPhrase = "No Content in the Request"
                    });
                }
            }
            catch (HttpResponseException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("Internal Server Error"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        // PUT: api/Product/5
        public async Task<bool> Put(ProductModel productModelToUpdate)
        {
            try
            {
                if (productModelToUpdate != null)
                {
                    Product productEntityToUpdate = Mapper.Map<Product>(productModelToUpdate);
                    return await ProductRepository.Update(productEntityToUpdate);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No Content in the Request"),
                        ReasonPhrase = "No Content in the Request"
                    });
                }
            }
            catch (HttpResponseException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("Internal Server Error"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        // DELETE: api/Product/5
        public async Task<bool> Delete(int id)
        {
            try
            {
                return await ProductRepository.Delete(id);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("Internal Server Error"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }
    }
}
