﻿using AutoMapper;
using John.Keells.Demo.Contract.Models;
using John.Keells.Demo.Domain.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace John.Keells.Demo.API.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterBindings()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Product, ProductModel>();
                config.CreateMap<ProductModel, Product>();
            });
        }
    }
}