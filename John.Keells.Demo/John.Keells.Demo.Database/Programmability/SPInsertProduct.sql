﻿CREATE PROCEDURE [dbo].[SPInsertProduct]
	@ProductName varchar(500),
	@ProductCode varchar(50),
	@Country varchar(500),
	@Price decimal,
	@IsDeleted bit
AS
	INSERT INTO Product (ProductName, Country, ProductCode, IsDeleted, Price) VALUES (@ProductName,@Country,@ProductCode,@IsDeleted,@Price);
RETURN 0
