﻿CREATE PROCEDURE [dbo].[SPRetrieveProduct]
	@ProductId int,
	@ProductCode varchar(50)
AS
	SELECT 
		[Product].ProductId,
		[Product].ProductName,
		[Product].ProductCode,
		[Product].Country,
		[Product].IsDeleted,
		[Product].Price
	FROM Product
	WHERE 
		(1 = CASE WHEN @ProductId IS NULL THEN 1 WHEN Product.ProductId = @ProductId THEN 1 ELSE 0 END) AND
		(1 = CASE WHEN @ProductCode IS NULL THEN 1 WHEN Product.ProductCode = @ProductCode THEN 1 ELSE 0 END) AND
		Product.IsDeleted = 0;
RETURN 0
