﻿CREATE PROCEDURE [dbo].[SPUpdateProduct]
	@ProductId int,
	@ProductName varchar(500),
	@ProductCode varchar(50),
	@Price decimal,
	@Country varchar(50)
AS
	UPDATE Product SET 
		ProductName = @ProductName,
		ProductCode = @ProductCode,
		Price = @Price,
		Country = @Country
	WHERE ProductId = @ProductId
RETURN 0
