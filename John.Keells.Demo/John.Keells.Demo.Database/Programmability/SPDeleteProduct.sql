﻿CREATE PROCEDURE [dbo].[SPDeleteProduct]
	@ProductId int
AS
	UPDATE Product SET IsDeleted = 1 WHERE ProductId = @ProductId;
RETURN 0
